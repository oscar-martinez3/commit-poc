# ADC Git commit message conventions

![banner](./docs/banner.jpg)

[![go](https://img.shields.io/badge/go-v1.13.X-blue.svg)](https://go.org/install/)

> A Golang tool to salute the world `:wq`
>
> Developed with all :heart: in the world by ADC I&O Cloud/DevOps team


## Table of Contents

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Contributing](#contributing)
- [Further Reading / Useful Links](#further-reading--useful-links)


## Prerequisites

You will need the following things properly installed on your computer if you want to *build/develop* `hello`:

* [git](http://git-scm.com/)
* [go](https://go.org/install/)

## Installation

### Build your own binary

#### Clone this repo

* `git clone git@bitbucket.org:oscar-martinez3/commit-poc.git` this repository.
* Change into the new directory `cd commit-poc`.

#### Generate the binary

To generate a binary named **hello** in the root of this repo, you will need to:

1. Build the binary for your OS:

```bash
# To build the binary...
$ go build
```

3. Enjoy :wink:


### Running hello

```bash
# To run hello...
$ ./hello
```

![run](./docs/run.png)
## Contributing

If you find this repo useful here's how you can help:

1. Send a Merge Request with your awesome new features and bug fixes
2. Wait for a Coronita :beer: you deserve it.

## Further Reading / Useful Links


* [Golang modules](https://blog.golang.org/using-go-modules)
